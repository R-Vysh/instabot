import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BotTopBarComponent } from './bot-top-bar/bot-top-bar.component';
import { BotInfoPageComponent } from './bot-info-page/bot-info-page.component';

@NgModule({
  declarations: [
    AppComponent,
    BotTopBarComponent,
    BotInfoPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
