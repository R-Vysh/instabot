import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-bot-info-page',
  templateUrl: './bot-info-page.component.html',
  styleUrls: ['./bot-info-page.component.css']
})
export class BotInfoPageComponent implements OnInit {

  @Input() botName;
  constructor() {
  }

  ngOnInit() {
  }
}
