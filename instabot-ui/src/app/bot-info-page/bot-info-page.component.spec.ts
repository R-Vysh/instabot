import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotInfoPageComponent } from './bot-info-page.component';

describe('BotInfoPageComponent', () => {
  let component: BotInfoPageComponent;
  let fixture: ComponentFixture<BotInfoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotInfoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotInfoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
