import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotTopBarComponent } from './bot-top-bar.component';

describe('BotTopBarComponent', () => {
  let component: BotTopBarComponent;
  let fixture: ComponentFixture<BotTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
