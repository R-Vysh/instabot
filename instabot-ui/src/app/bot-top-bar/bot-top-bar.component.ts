import{Component, OnInit}from '@angular/core';

import {bots}from '../bots';

@Component({
selector: 'app-bot-top-bar',
templateUrl: './bot-top-bar.component.html',
styleUrls: ['./bot-top-bar.component.css']
})
export class BotTopBarComponent implements OnInit {

savedBots = bots;

constructor() { }

  ngOnInit() {
  }

  start() {
    console.log('Start');
  }

  restart() {
    console.log('ReStart');
  }

  addBot() {
    console.log('ADD BOT');
  }

  onBotChange() {
    console.log('onBotChange');
  }
}
