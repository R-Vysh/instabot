package ua.kyiv.rvysh.instabot.configuration;

import clarifai2.api.ClarifaiBuilder;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.kyiv.rvysh.instabot.driver.DriverFactory;
import ua.kyiv.rvysh.instabot.driver.DriverType;
import ua.kyiv.rvysh.instabot.filter.FilterChain;
import ua.kyiv.rvysh.instabot.filter.StopWordFilter;
import ua.kyiv.rvysh.instabot.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class ServiceConfiguration {
    @Autowired
    private RepositoryConfiguration repoConfig;

    @Value("${follow.pause.before.min}")
    private int prefollowPauseMin;

    @Value("${follow.pause.before.random}")
    private int prefollowPauseRandom;

    @Value("${follow.pause.after.min}")
    private int postfollowPauseMin;

    @Value("${follow.pause.after.random}")
    private int postfollowPauseRandom;

    @Value("${unfollow.pause.min}")
    private int unfollowPauseMin;

    @Value("${unfollow.pause.random}")
    private int unfollowPauseRandom;

    @Value("${selenium.driver}")
    private DriverType driverType;

    @Value("${clarifai.api.key}")
    private String clarifaiKey;

    @Bean
    public CookiesService cookiesService() {
        return new CookiesService(repoConfig.cookiesDao());
    }

    @Bean
    public FollowersService followersService() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return new FollowersService(mapper, new Random(), prefollowPauseMin, prefollowPauseRandom, postfollowPauseMin,
                postfollowPauseRandom, unfollowPauseMin, unfollowPauseRandom, lockService());
    }

    @Bean
    public LikeService likeService() {
        return new LikeService(lockService());
    }

    @Bean
    public CountersService countersService() {
        return new CountersService(repoConfig.countersDao());
    }

    @Bean
    public LoginService loginService() {
        return new LoginService(cookiesService(), new Random());
    }

    @Bean
    public PostService postService() {
        return new PostService(lockService());
    }

    @Bean
    public UnfollowService unfollowService() {
        return new UnfollowService(repoConfig.unfollowDao());
    }

    @Bean
    public QueueService queueService() {
        return new QueueService(repoConfig.queueRepository());
    }

    @Bean
    public LockService lockService() {
        return new LockService();
    }

    @Bean
    public BotService botService() {
        return new BotService(followersService(), countersService(), loginService(),
                unfollowService(), filterChain(), queueService(),
                driverFactory(), repoConfig.botRepository(), driverType);
    }

    @Bean
    public ClarifaiService clarifaiService() {
        return new ClarifaiService(new ClarifaiBuilder(clarifaiKey).buildSync());
    }

    @Bean
    public DriverFactory driverFactory() {
        return new DriverFactory();
    }

    @Bean
    public FilterChain filterChain() {
        List<String> stopWords = new ArrayList<>();
        return new FilterChain.Builder().addFilter(new StopWordFilter(stopWords)).build();
    }
}
