package ua.kyiv.rvysh.instabot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.core.har.HarEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.domain.Follower;
import ua.kyiv.rvysh.instabot.domain.FollowerNode;
import ua.kyiv.rvysh.instabot.domain.exception.BotRestartException;
import ua.kyiv.rvysh.instabot.driver.DriverBundle;
import ua.kyiv.rvysh.instabot.filter.FilterChain;
import ua.kyiv.rvysh.instabot.utils.Constants;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FollowersService {
    private static final Logger LOG = LoggerFactory.getLogger(FollowersService.class);
    private static final String UNFOLLOW_BTN_XPATH = "(//span[@aria-label='Following'] | //button[text()='Requested'])[1]";
    private static final String UNFOLLOW_CONFIRM_XPATH = "//button[text()='Unfollow']";
    private static final String FOLLOW_BTN_XPATH = "//button[text()='Follow']/..";

    private final ObjectMapper mapper;
    private final Random random;
    private final int prefollowPauseMin;
    private final int prefollowPauseRandom;
    private final int postfollowPauseMin;
    private final int postfollowPauseRandom;
    private final int unfollowPauseMin;
    private final int unfollowPauseRandom;

    private final LockService lockService;

    public FollowersService(ObjectMapper mapper, Random random, int prefollowPauseMin, int prefollowPauseRandom,
                            int postfollowPauseMin, int postfollowPauseRandom, int unfollowPauseMin, int unfollowPauseRandom,
                            LockService lockService) {
        this.mapper = mapper;
        this.random = random;
        this.prefollowPauseMin = prefollowPauseMin;
        this.prefollowPauseRandom = prefollowPauseRandom;
        this.postfollowPauseMin = postfollowPauseMin;
        this.postfollowPauseRandom = postfollowPauseRandom;
        this.unfollowPauseMin = unfollowPauseMin;
        this.unfollowPauseRandom = unfollowPauseRandom;
        this.lockService = lockService;
    }

    private List<Follower> getPeople(InstaBot bot, String user, boolean queryFollowers, int limit) throws InterruptedException {
        lockService.lock(bot);
        DriverBundle driverBundle = bot.getDriverBundle();
        WebDriverWait wait = driverBundle.getWait();
        WebDriver driver = driverBundle.getDriver();
        BrowserMobProxy proxy = driverBundle.getProxy();

        Set<Follower> result = new LinkedHashSet<>();
        LOG.info("Fetching people for {}", user);
        String uriSuffix;
        if (queryFollowers) {
            uriSuffix = "followers";
        } else {
            uriSuffix = "following";
        }
        proxy.newHar(user);
        driver.get(Constants.BASE_URL + user);

        WebElement peopleBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//a[@href='/" + user + "/" + uriSuffix + "/']/..")));
        TimeUnit.SECONDS.sleep(5);

        peopleBtn.click();
        int previousCount = -1;
        while (result.size() < limit && previousCount < result.size()) {
            previousCount = result.size();
            TimeUnit.SECONDS.sleep(10);
            result.addAll(parseResponses(proxy, queryFollowers, user));
        }
        LOG.info("Finished fetching people for {}", user);
        lockService.unlock(bot);
        driver.get(Constants.BASE_URL);
        return new ArrayList<>(result);
    }

    private List<Follower> parseResponses(BrowserMobProxy proxy, boolean queryFollowers,
                                          String user) {
        List<Follower> res = new ArrayList<>();
        List<HarEntry> allRequests = ImmutableList.copyOf(proxy.getHar().getLog().getEntries());
        proxy.getHar().getLog().getEntries().clear();
        List<HarEntry> requests = allRequests.stream().filter(
                e -> e.getRequest().getUrl().startsWith("https://www.instagram.com/graphql/query/"))
                .collect(Collectors.toList());
        for (HarEntry req : requests) {
            String body = req.getResponse().getContent().getText();
            try {
                // TODO check redundant users
                JsonNode userDataNode = mapper.readTree(body).get("data").get("user");
                JsonNode followersNode;
                if (queryFollowers) {
                    followersNode = userDataNode.has("edge_chaining")
                            ? userDataNode.get("edge_chaining").withArray("edges")
                            : userDataNode.get("edge_followed_by").withArray("edges");
                } else {
                    if (userDataNode.has("edge_follow")) {
                        followersNode = userDataNode.get("edge_follow").withArray("edges");
                    } else {
                        LOG.debug("Skipping body {}", body);
                        continue;
                    }
                }
                FollowerNode[] followerNodes = mapper.treeToValue(followersNode,
                        FollowerNode[].class);
                List<Follower> followers = Arrays.asList(followerNodes).stream()
                        .map(e -> e.getNode()).collect(Collectors.toList());
                res.addAll(followers);
            } catch (IOException | NullPointerException ex) {
                LOG.error("Failed to parse followers, body: {}", body, ex);
            }
        }
        proxy.newHar(user);
        return res;
    }

    public List<Follower> getFollowers(InstaBot bot, String user, int size)
            throws InterruptedException {
        return getPeople(bot, user, true, size);
    }

    public List<Follower> getFollowings(InstaBot bot, int size) throws InterruptedException {
        return getPeople(bot, bot.getAccountName(), false, size);
    }

    public boolean follow(InstaBot bot, String user, FilterChain filters) {
        lockService.lock(bot);
        try {
            DriverBundle driverBundle = bot.getDriverBundle();
            WebDriver driver = driverBundle.getDriver();
            WebDriverWait wait = driverBundle.getWait();
            driver.get(Constants.BASE_URL + user);
            if (driver.findElements(By.xpath(UNFOLLOW_BTN_XPATH)).size() != 0) {
                return false;
            }
            List<WebElement> spans = driver.findElements(By.xpath("//h1/following-sibling::span"));
            if (spans.size() < 2) {
                // no description - no filter
            } else {
                String description = spans.get(1).getText();
                if (!filters.filter(description)) {
                    LOG.info("Filtered out {}", user);
                    return false;
                }
            }
            TimeUnit.SECONDS.sleep(random.nextInt(prefollowPauseRandom) + prefollowPauseMin);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FOLLOW_BTN_XPATH)))
                    .click();
            TimeUnit.SECONDS.sleep(random.nextInt(postfollowPauseRandom) + postfollowPauseMin);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(UNFOLLOW_BTN_XPATH)));
            LOG.info("Followed {}", user);
            return true;
        } catch (Exception e) {
            LOG.error("Failed to follow {}", user, e);
            return false;
        } finally {
            lockService.unlock(bot);
        }
    }

    public void unfollow(InstaBot bot, List<String> users)
            throws InterruptedException, BotRestartException {
        for (String user : users) {
            unfollow(bot, user);
        }
        LOG.info("Finished unfollowing for {}", bot.getAccountName());
    }

    public void unfollow(InstaBot bot, String user) throws BotRestartException {
        lockService.lock(bot);
        try {
            DriverBundle driverBundle = bot.getDriverBundle();
            WebDriver driver = driverBundle.getDriver();
            WebDriverWait wait = driverBundle.getWait();
            driver.get(Constants.BASE_URL + user);
            if (driver.findElements(By.xpath(FOLLOW_BTN_XPATH)).size() != 0) {
                return;
            }
            TimeUnit.SECONDS.sleep(random.nextInt(2) + 1);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(UNFOLLOW_BTN_XPATH)))
                    .click();
            TimeUnit.SECONDS.sleep(2);
            // If confirm unfollow appears
            List<WebElement> unfollowConfirm = driver
                    .findElements(By.xpath(UNFOLLOW_CONFIRM_XPATH));
            if (unfollowConfirm.size() != 0) {
                unfollowConfirm.get(0).click();
            }
            TimeUnit.SECONDS.sleep(random.nextInt(unfollowPauseRandom) + unfollowPauseMin);
            LOG.info("Unfollowed {}", user);
        } catch (TimeoutException e) {
            LOG.error("Failed to unfollow due to timeout, page {}", user);
            throw new BotRestartException();
        } catch (Exception e) {
            LOG.error("Failed to unfollow {}", user, e);
        } finally {
            lockService.unlock(bot);
        }
    }
}