package ua.kyiv.rvysh.instabot;

import ua.kyiv.rvysh.instabot.driver.DriverBundle;

public class InstaBot {
    private final String accountName;
    private final DriverBundle driverBundle;
    private final int followLimit;

    private char[] password;

    public InstaBot(String accountName, DriverBundle driverBundle, int followLimit) {
        this(accountName, new char[]{}, driverBundle, followLimit);
    }

    public InstaBot(String accountName, char[] password, DriverBundle driverBundle, int followLimit) {
        this.accountName = accountName;
        this.password = password;
        this.driverBundle = driverBundle;
        this.followLimit = followLimit;
    }

    public void shutdown() {
        driverBundle.getProxy().stop();
        driverBundle.getDriver().close();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public char[] getPassword() {
        return password;
    }

    public DriverBundle getDriverBundle() {
        return driverBundle;
    }

    public int getFollowLimit() {
        return followLimit;
    }
}
