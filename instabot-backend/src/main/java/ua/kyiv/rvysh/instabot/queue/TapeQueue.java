package ua.kyiv.rvysh.instabot.queue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.squareup.tape.QueueFile;

public class TapeQueue {
	private final QueueFile queueFile;
	private final String name;

	public TapeQueue(QueueFile queueFile, String name) {
		this.queueFile = queueFile;
		this.name = name;
	}

	public void addAll(Collection<? extends String> elements) throws IOException {
		for (String s : elements) {
			add(s);
		}
	}

	public void clear() throws IOException {
		queueFile.clear();
	}

	public boolean isEmpty() {
		return queueFile.isEmpty();
	}

	public int size() {
		return queueFile.size();
	}

	public void add(String s) throws IOException {
		queueFile.add(s.getBytes(StandardCharsets.UTF_8));
	}

	public String peek() throws IOException {
		byte[] b = queueFile.peek();
		return b == null ? null : new String(b, StandardCharsets.UTF_8);
	}

	public List<String> peek(int n) throws IOException {
		List<String> res = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			String el = peek();
			if (el != null) {
				res.add(el);
			}
		}
		return res;
	}

	public String poll() throws IOException {
		String el = peek();
		if (el != null) {
			queueFile.remove();
		}
		return el;
	}

	public List<String> poll(int n) throws IOException {
		List<String> res = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			String el = poll();
			if (el != null) {
				res.add(el);
			}
		}
		return res;
	}
	
	public String getName() {
		return name;
	}
}
