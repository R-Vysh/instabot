package ua.kyiv.rvysh.instabot.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class RobotUtils {

	static {
		System.setProperty("java.awt.headless", "false");
	}

	private static final Robot robot = createRobot();
	private static final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

	private static Robot createRobot() {
		try {
			return new Robot();
		} catch (AWTException e) {
			throw new RuntimeException(e);
		}
	}

	private RobotUtils() {
	}

	public static synchronized void typeOnKeyboard(String text, boolean pressEnter) {
		StringSelection stringSelection = new StringSelection(text);
		clipboard.setContents(stringSelection, stringSelection);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		if (pressEnter) {
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		}
	}
}
