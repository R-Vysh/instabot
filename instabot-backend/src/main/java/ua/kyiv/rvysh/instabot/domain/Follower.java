package ua.kyiv.rvysh.instabot.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Follower {
	private long id;

	@JsonProperty("blocked_by_viewer")
	private boolean blockedByViewer;

	@JsonProperty("followed_by_viewer")
	private boolean followedByViewer;

	@JsonProperty("follows_viewer")
	private boolean followsViewer;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("has_blocked_viewer")
	private boolean blockedViewer;

	@JsonProperty("has_requested_viewer")
	private boolean requestedViewer;

	@JsonProperty("is_private")
	private boolean privateAcc;

	@JsonProperty("is_verified")
	private boolean verified;

	@JsonProperty("profile_pic_url")
	private String picUrl;

	@JsonProperty("requested_by_viewer")
	private boolean requestedByViewer;

	private String username;

	public boolean shouldFollow() {
		return !blockedByViewer 
				&& !followedByViewer 
				&& !followsViewer 
				&& !blockedViewer
				&& !requestedViewer 
				&& !requestedByViewer;
	}
	
	public boolean shouldUnfollow() {
		return followedByViewer || requestedByViewer;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isBlockedByViewer() {
		return blockedByViewer;
	}

	public void setBlockedByViewer(boolean blockedByViewer) {
		this.blockedByViewer = blockedByViewer;
	}

	public boolean isFollowedByViewer() {
		return followedByViewer;
	}

	public void setFollowedByViewer(boolean followedByViewer) {
		this.followedByViewer = followedByViewer;
	}

	public boolean isFollowsViewer() {
		return followsViewer;
	}

	public void setFollowsViewer(boolean followsViewer) {
		this.followsViewer = followsViewer;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isBlockedViewer() {
		return blockedViewer;
	}

	public void setBlockedViewer(boolean blockedViewer) {
		this.blockedViewer = blockedViewer;
	}

	public boolean isRequestedViewer() {
		return requestedViewer;
	}

	public void setRequestedViewer(boolean requestedViewer) {
		this.requestedViewer = requestedViewer;
	}

	public boolean isPrivateAcc() {
		return privateAcc;
	}

	public void setPrivateAcc(boolean privateAcc) {
		this.privateAcc = privateAcc;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public boolean isRequestedByViewer() {
		return requestedByViewer;
	}

	public void setRequestedByViewer(boolean requestedByViewer) {
		this.requestedByViewer = requestedByViewer;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public String toString() {
		return "[id=" + id + ", username=" + username + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Follower other = (Follower) obj;
		if (id != other.id)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}
