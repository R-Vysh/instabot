package ua.kyiv.rvysh.instabot.service;

import clarifai2.api.ClarifaiClient;
import clarifai2.api.ClarifaiResponse;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.model.ConceptModel;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Prediction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ClarifaiService {
    private static final Logger LOG = LoggerFactory.getLogger(ClarifaiService.class);

    private final ClarifaiClient client;
    private final ConceptModel generalModel;

    public ClarifaiService(ClarifaiClient client) {
        this.client = client;
        this.generalModel = client.getDefaultModels().generalModel();
    }

    public List<ClarifaiOutput<Prediction>> predict(String url) {
        ClarifaiResponse<List<ClarifaiOutput<Prediction>>> response = client.predict(generalModel.id())
                .withInputs(ClarifaiInput.forImage(url))
                .executeSync();
        if (!response.isSuccessful()) {
            LOG.warn("Failed to Clarifai, status code:{}", response.getStatus().statusCode());
            return null;
        } else {
            return response.get();
        }
    }
}
