package ua.kyiv.rvysh.instabot.domain;

import java.time.ZonedDateTime;

public class Cookiez {
    private final String sessionId;
    private final ZonedDateTime expiresOn;

    public Cookiez(String sessionId, ZonedDateTime expiresOn) {
        this.sessionId = sessionId;
        this.expiresOn = expiresOn;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ZonedDateTime getExpiresOn() {
        return expiresOn;
    }
}
