package ua.kyiv.rvysh.instabot.filter;

import java.util.List;

public class StopWordFilter implements Filter {

	private List<String> stopWords;

	public StopWordFilter(List<String> stopWords) {
		this.stopWords = stopWords;
	}

	@Override
	public boolean filter(String description) {
		for (String stopWord : stopWords) {
			if (description.toLowerCase().contains(stopWord.toLowerCase())) {
				return false;
			}
		}
		return true;
	}
}
