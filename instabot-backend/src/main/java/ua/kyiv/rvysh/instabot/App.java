package ua.kyiv.rvysh.instabot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import ua.kyiv.rvysh.instabot.configuration.ServiceConfiguration;
import ua.kyiv.rvysh.instabot.controllers.QueuesController;

@ComponentScan(basePackageClasses = {
		ServiceConfiguration.class,
		QueuesController.class
})
@SpringBootApplication
@EnableScheduling
public class App implements CommandLineRunner {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(App.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}

}
