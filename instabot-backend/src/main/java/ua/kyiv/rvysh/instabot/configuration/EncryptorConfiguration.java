package ua.kyiv.rvysh.instabot.configuration;

import org.jasypt.util.text.StrongTextEncryptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EncryptorConfiguration {

	@Value("${jasypt.password}")
	private String jasyptPass;
	
	@Bean
	public StrongTextEncryptor textEncryptor() {
		StrongTextEncryptor encryptor = new StrongTextEncryptor();
		encryptor.setPassword(jasyptPass);
		return encryptor;
	}
}
