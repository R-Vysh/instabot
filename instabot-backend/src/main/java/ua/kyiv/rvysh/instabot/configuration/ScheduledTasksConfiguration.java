package ua.kyiv.rvysh.instabot.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ua.kyiv.rvysh.instabot.scheduled.ScheduledTasks;

@Configuration
public class ScheduledTasksConfiguration {
	@Autowired
	private ServiceConfiguration serviceConfig;

	@Bean
	public ScheduledTasks scheduledTasks() {
		return new ScheduledTasks(serviceConfig.unfollowService(), serviceConfig.queueService());
	}
}
