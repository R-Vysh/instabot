package ua.kyiv.rvysh.instabot.repository;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.squareup.tape.QueueFile;

import ua.kyiv.rvysh.instabot.queue.TapeQueue;

public class QueueRepository {
	private static final Logger LOG = LoggerFactory.getLogger(QueueRepository.class);

	private final String baseFolder;

	public QueueRepository(String baseFolder) {
		this.baseFolder = baseFolder;
		try {
			Files.createDirectories(Paths.get(baseFolder));
		} catch (IOException e) {
			LOG.error("Failed to create path to queues folder");
			throw new RuntimeException(e);
		}
	}

	public TapeQueue getQueue(String name) throws IOException {
		QueueFile queueFile = new QueueFile(new File(baseFolder + name));
		return new TapeQueue(queueFile, name);
	}

	public List<TapeQueue> getQueues(String botName) throws IOException {
		List<TapeQueue> res = new ArrayList<>();
		File dir = new File(baseFolder);
		File[] queues = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(botName + "-");
			}
		});
		for (File queue : queues) {
			res.add(new TapeQueue(new QueueFile(queue), queue.getName()));
		}
		return res;
	}

	public void deleteQueue(String name) throws IOException {
		new File(baseFolder + name).delete();
	}
}
