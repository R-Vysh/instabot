package ua.kyiv.rvysh.instabot.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ua.kyiv.rvysh.instabot.repository.BotRepository;
import ua.kyiv.rvysh.instabot.repository.CookiesDao;
import ua.kyiv.rvysh.instabot.repository.CountersDao;
import ua.kyiv.rvysh.instabot.repository.FollowersDao;
import ua.kyiv.rvysh.instabot.repository.QueueRepository;
import ua.kyiv.rvysh.instabot.repository.UnfollowDao;

@Configuration
public class RepositoryConfiguration {

    @Autowired
    private DataSourceConfiguration dataSourceConfig;

    @Autowired
    private EncryptorConfiguration encryptorConfig;

    @Bean
    public CookiesDao cookiesDao() {
        return new CookiesDao(dataSourceConfig.jooqContext(), encryptorConfig.textEncryptor());
    }

    @Bean
    public FollowersDao followersDao() {
        return new FollowersDao(dataSourceConfig.jdbcTemplate());
    }

    @Bean
    public CountersDao countersDao() {
        return new CountersDao(dataSourceConfig.jooqContext());
    }

    @Bean
    public UnfollowDao unfollowDao() {
        return new UnfollowDao(dataSourceConfig.jdbcTemplate(), dataSourceConfig.jooqContext());
    }

    @Bean
    public QueueRepository queueRepository() {
        return new QueueRepository("queues/");
    }

    @Bean
    public BotRepository botRepository() {
        return new BotRepository(dataSourceConfig.jooqContext());
    }
}
