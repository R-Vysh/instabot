package ua.kyiv.rvysh.instabot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(1)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${auth.enabled}")
    private boolean authEnabled;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll();
        if (authEnabled) {
            http.authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginProcessingUrl("/login")
                    .loginPage("/signin.html")
                    .permitAll();
        } else {
            http.authorizeRequests()
                    .anyRequest().permitAll();
        }
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        // TODO sqlite based
        builder.inMemoryAuthentication()
                .withUser("user").password(passwordEncoder().encode("passwd121!")).roles("USER")
                .and().withUser("admin").password(passwordEncoder().encode("passwd121!")).roles("ADMIN");
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(16);
    }
}
