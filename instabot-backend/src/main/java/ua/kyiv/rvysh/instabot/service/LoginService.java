package ua.kyiv.rvysh.instabot.service;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.domain.Cookiez;
import ua.kyiv.rvysh.instabot.domain.exception.BotLoginException;
import ua.kyiv.rvysh.instabot.driver.DriverBundle;
import ua.kyiv.rvysh.instabot.utils.Constants;

import java.nio.CharBuffer;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class LoginService {
    private static final Logger LOG = LoggerFactory.getLogger(LoginService.class);

    private final CookiesService cookiesService;
    private final Random random;

    public LoginService(CookiesService cookiesService, Random random) {
        this.cookiesService = cookiesService;
        this.random = random;
    }

    public void login(InstaBot bot) throws BotLoginException {
        DriverBundle driverWait = bot.getDriverBundle();
        String accName = bot.getAccountName();
        CharSequence pass = CharBuffer.wrap(bot.getPassword());
        bot.setPassword("".toCharArray());
        WebDriver driver = driverWait.getDriver();
        WebDriverWait wait = driverWait.getWait();
        try {
            driver.get(Constants.BASE_URL);
            Cookiez cookies = cookiesService.getCookies(accName);
            if (cookies != null && cookies.getExpiresOn().isAfter(ZonedDateTime.now())) {
                Date expires = Date.from(cookies.getExpiresOn().toInstant());
                Cookie ck = new Cookie("sessionid", cookies.getSessionId(), "/", expires);
                driver.manage().addCookie(ck);
                driver.get(Constants.BASE_URL);
                if (checkLoggedIn(driver, accName)) {
                    return;
                }
            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("(//button[text()='Log In'] | //button[text()='Log in'])[1]")))
                    .click();
            TimeUnit.SECONDS.sleep(random.nextInt(3) + 3);
            WebElement nameInput = wait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.cssSelector("input[name='username']")));
            nameInput.click();
            nameInput.sendKeys(accName);
            WebElement passInput = wait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.cssSelector("input[name='password']")));
            passInput.click();
            passInput.sendKeys(pass);
            pass = null;
            TimeUnit.SECONDS.sleep(1);
            wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("(//div[text()='Log In'] | //div[text()='Log in'] | //button[text()='Log In'] | //button[text()='Log in'])[1]")))
                    .click();
            TimeUnit.SECONDS.sleep(random.nextInt(3) + 2);
            Cookie sessionCookie = driver.manage().getCookieNamed("sessionid");
            if (sessionCookie != null) {
                saveCookies(accName, sessionCookie);
            } else {
                wait.until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("(//div[text()='Send Security Code'] | //button[text()='Send Security Code'])[1]")))
                        .click();
                while (driver.manage().getCookieNamed("sessionid") == null) {
                    LOG.info("Waiting for security code input");
                    TimeUnit.SECONDS.sleep(10);
                }
                saveCookies(accName, driver.manage().getCookieNamed("sessionid"));
            }
        } catch (Exception ex) {
            LOG.error("Failed to login {}", accName);
            driver.close();
            throw new BotLoginException();
        }
    }

    private void saveCookies(String accName, Cookie sessionCookie) {
        ZonedDateTime expires = ZonedDateTime.ofInstant(sessionCookie.getExpiry().toInstant(), ZoneId.systemDefault());
        cookiesService.saveCookies(accName, sessionCookie.getValue(), expires);
    }

    private boolean checkLoggedIn(WebDriver driver, String accountName)
            throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return driver.findElements(By.cssSelector("a[href='/" + accountName + "/']")).size() != 0;
    }
}
