package ua.kyiv.rvysh.instabot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.domain.BotCredentials;
import ua.kyiv.rvysh.instabot.domain.Follower;
import ua.kyiv.rvysh.instabot.domain.exception.BotLoginException;
import ua.kyiv.rvysh.instabot.domain.exception.BotRestartException;
import ua.kyiv.rvysh.instabot.driver.DriverFactory;
import ua.kyiv.rvysh.instabot.driver.DriverType;
import ua.kyiv.rvysh.instabot.filter.FilterChain;
import ua.kyiv.rvysh.instabot.queue.TapeQueue;
import ua.kyiv.rvysh.instabot.repository.BotRepository;
import ua.kyiv.rvysh.instabot.utils.QueueUtils;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BotService {
    private static final Logger LOG = LoggerFactory.getLogger(BotService.class);

    private final Map<String, InstaBot> runningBots;

    private final FollowersService followersService;
    private final CountersService countersService;
    private final LoginService loginService;
    private final UnfollowService unfollowService;
    private final FilterChain filters;
    private final QueueService queues;
    private final DriverFactory driverFactory;
    private final BotRepository botRepo;
    private final DriverType driverType;

    public BotService(FollowersService followersService, CountersService countersService,
                      LoginService loginService, UnfollowService unfollowService, FilterChain filters,
                      QueueService queues, DriverFactory driverFactory, BotRepository botRepository,
                      DriverType type) {
        this.followersService = followersService;
        this.countersService = countersService;
        this.loginService = loginService;
        this.unfollowService = unfollowService;
        this.filters = filters;
        this.queues = queues;
        this.driverFactory = driverFactory;
        this.botRepo = botRepository;
        this.driverType = type;
        this.runningBots = new HashMap<>();
    }

    public synchronized void addBot(BotCredentials creds, int followLimit)
            throws BotLoginException {
        String user = creds.getAccName();
        LOG.info("Creating new bot {}", user);
        // If bot is already saved then just update cookies
        InstaBot bot = new InstaBot(user, creds.getPassword(), driverFactory.getDriver(driverType), followLimit);
        loginService.login(bot);
        if (!botRepo.checkSaved(user)) {
            botRepo.save(bot);
        }
        bot.shutdown();
    }

    public synchronized InstaBot startBot(String user) throws BotLoginException {
        LOG.info("Starting bot {}", user);
        if (botRepo.checkSaved(user)) {
            InstaBot bot = new InstaBot(user, driverFactory.getDriver(driverType), botRepo.getDailyLimit(user));
            loginService.login(bot);
            runningBots.put(bot.getAccountName(), bot);
            return bot;
        } else {
            throw new BotLoginException();
        }
    }

    public InstaBot get(String name) {
        return runningBots.get(name);
    }

    public Set<String> listAll() {
        return botRepo.getAll();
    }

    public Set<String> listRunning() {
        return runningBots.keySet();
    }

    public void shutdown(InstaBot bot) {
        bot.shutdown();
        runningBots.remove(bot.getAccountName());
    }

    public void restart(InstaBot bot) throws BotLoginException {
        LOG.info("Restarting bot {}", bot.getAccountName());
        shutdown(bot);
        startBot(bot.getAccountName());
    }

    public void enqueueFollowers(InstaBot bot, String donorPage, int size)
            throws InterruptedException, IOException {
        LOG.info("Getting followers of {}", donorPage);
        List<Follower> followers = followersService.getFollowers(bot, donorPage, size);
        List<String> usersToFollow = followers.stream().filter(e -> e.shouldFollow())
                .map(e -> e.getUsername()).collect(Collectors.toList());
        LOG.info("Queing followers of {} for {}", donorPage, bot.getAccountName());
        queues.getQueue(bot.getAccountName() + "-" + donorPage).addAll(usersToFollow);
    }

    public void enqueueUnfollow(InstaBot bot, int size) throws InterruptedException, IOException {
        LOG.info("Starting unfollow queueing {}", bot.getAccountName());
        List<Follower> unfollow = followersService.getFollowings(bot, size);
        List<String> usersToUnfollow = unfollow.stream().filter(e -> e.shouldUnfollow())
                .map(e -> e.getUsername()).collect(Collectors.toList());
        LOG.info("Queing unfollow for {}", bot.getAccountName());
        queues.getQueue(QueueUtils.getUnfollowQueueName(bot)).addAll(usersToUnfollow);
    }

    public void processUnfollowQueue(InstaBot bot) throws IOException {
        try {
            int todayCount = countersService.getUnfollowCount(bot);
            TapeQueue queue = queues.getQueue(QueueUtils.getUnfollowQueueName(bot));
            LOG.info("Processing unfollow queue: {}", bot.getAccountName());
            while (!queue.isEmpty()) {
                List<String> users = queue.poll(1);
                for (String user : users) {
                    followersService.unfollow(bot, user);
                    todayCount++;
                    countersService.insertUnfollowCount(bot, todayCount);
                    // TODO wtf???
//                queue.add(user);
                }
            }
            LOG.info("Finished processing unfollow queue: {}", bot.getAccountName());
        } catch (BotRestartException e) {
            try {
                restart(bot);
            } catch (BotLoginException e1) {
                LOG.error("Failed to restart bot {}", bot.getAccountName());
                return;
            }
            processUnfollowQueue(bot);
        }
    }

    public void processFollowQueues(InstaBot bot) throws IOException {
        int todayCount = countersService.getFollowingCount(bot);
        List<TapeQueue> qus = queues.getQueues(bot).stream()
                .filter(q -> !q.getName().equals(QueueUtils.getUnfollowQueueName(bot)))
                .collect(Collectors.toList());
        for (TapeQueue queue : qus) {
            LOG.info("Processing queue: {}-{}", bot.getAccountName(), queue.getName());
            while (!queue.isEmpty()) {
                List<String> users = queue.poll(1);
                int processedCount = 0;
                for (String user : users) {
                    if (todayCount < bot.getFollowLimit()) {
                        if (followersService.follow(bot, user, filters)) {
                            todayCount++;
                            countersService.insertFollowingCount(bot, todayCount);
                            unfollowService.insertUnfollow(bot, user);
                        }
                        processedCount++;
                    } else {
                        // Put back the remaining of the batch
                        queue.addAll(users.subList(processedCount, users.size()));
                        LOG.info("Daily limit for {} reached", bot.getAccountName());
                        return;
                    }
                }
            }
            LOG.info("Finished processing queue: {}-{}", bot.getAccountName(), queue.getName());
        }
    }
}
