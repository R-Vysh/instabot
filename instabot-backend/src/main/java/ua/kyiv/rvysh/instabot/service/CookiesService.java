package ua.kyiv.rvysh.instabot.service;

import ua.kyiv.rvysh.instabot.domain.Cookiez;
import ua.kyiv.rvysh.instabot.repository.CookiesDao;

import java.time.ZonedDateTime;

public class CookiesService {
    private final CookiesDao cookiesDao;

    public CookiesService(CookiesDao dao) {
        this.cookiesDao = dao;
    }

    public Cookiez getCookies(String user) {
        return cookiesDao.getCookies(user);
    }

    public void saveCookies(String user, String cookies, ZonedDateTime expiresOn) {
        cookiesDao.insertCookies(user, cookies, expiresOn);
    }

    public boolean isCookieValid(String user) {
        Cookiez cookiez = getCookies(user);
        if (cookiez == null) {
            return false;
        } else {
            if (cookiez.getExpiresOn().isAfter(ZonedDateTime.now())) {
                return true;
            } else {
                return false;
            }
        }
    }
}
