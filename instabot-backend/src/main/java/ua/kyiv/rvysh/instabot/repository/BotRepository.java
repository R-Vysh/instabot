package ua.kyiv.rvysh.instabot.repository;

import org.jooq.DSLContext;
import ua.kyiv.rvysh.instabot.InstaBot;

import java.util.HashSet;
import java.util.Set;

import static ua.kyiv.rvysh.instabot.domain.generated.Tables.BOTS;

public class BotRepository {
    private final DSLContext dslContext;

    public BotRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public void save(InstaBot bot) {
        dslContext.insertInto(BOTS, BOTS.NAME, BOTS.MAX_FOLLOW_PER_DAY)
                .values(bot.getAccountName(), bot.getFollowLimit())
                .onConflict(BOTS.NAME)
                .doUpdate()
                .set(BOTS.MAX_FOLLOW_PER_DAY, bot.getFollowLimit())
                .execute();
    }

    public boolean checkSaved(String name) {
        return dslContext.select(BOTS.ID).from(BOTS).where(BOTS.NAME.eq(name)).fetchOne() != null;

    }

    public Set<String> getAll() {
        Set<String> result = new HashSet<>();
        dslContext.select(BOTS.NAME).from(BOTS).fetch().stream().forEach(row -> result.add(row.value1()));
        return result;
    }

    public void remove(String name) {
        dslContext.delete(BOTS).where(BOTS.NAME.eq(name));
    }

    public int getDailyLimit(String user) {
        return dslContext.select(BOTS.MAX_FOLLOW_PER_DAY)
                .from(BOTS)
                .where(BOTS.NAME.eq(user))
                .fetchOne()
                .get(BOTS.MAX_FOLLOW_PER_DAY);
    }
}
