package ua.kyiv.rvysh.instabot.utils;

import ua.kyiv.rvysh.instabot.InstaBot;

public class QueueUtils {
    private static final String UNFOLLOW_Q_SUFFIX = "-unfollow";

    private QueueUtils() {
    }

    public static String getUnfollowQueueName(InstaBot bot) {
        return getUnfollowQueueName(bot.getAccountName());
    }

    public static String getUnfollowQueueName(String botName) {
        return botName + UNFOLLOW_Q_SUFFIX;
    }
}
