package ua.kyiv.rvysh.instabot.service;

import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.domain.QueueInfo;
import ua.kyiv.rvysh.instabot.queue.TapeQueue;
import ua.kyiv.rvysh.instabot.repository.QueueRepository;
import ua.kyiv.rvysh.instabot.utils.QueueUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class QueueService {
    private final QueueRepository queueRepository;

    public QueueService(QueueRepository queueRepository) {
        this.queueRepository = queueRepository;
    }

    public TapeQueue getQueue(String name) throws IOException {
        return queueRepository.getQueue(name);
    }

    public TapeQueue getUnfollowQueue(InstaBot bot) throws IOException {
        return getUnfollowQueue(bot.getAccountName());
    }

    public TapeQueue getUnfollowQueue(String botName) throws IOException {
        return queueRepository.getQueue(QueueUtils.getUnfollowQueueName(botName));
    }

    public List<TapeQueue> getQueues(InstaBot bot) throws IOException {
        return getQueues(bot.getAccountName());
    }

    public List<TapeQueue> getQueues(String botName) throws IOException {
        return queueRepository.getQueues(botName);
    }

    public List<QueueInfo> getQueuesInfo(InstaBot bot) throws IOException {
        return getQueuesInfo(bot.getAccountName());
    }

    public List<QueueInfo> getQueuesInfo(String botName) throws IOException {
        return getQueues(botName).stream().map(e -> new QueueInfo(e.getName(), e.size()))
                .collect(Collectors.toList());
    }
}
