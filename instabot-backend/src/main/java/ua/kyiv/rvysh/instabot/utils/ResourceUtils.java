package ua.kyiv.rvysh.instabot.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

public class ResourceUtils {
	private ResourceUtils() {
	}

	public static String toString(Resource resource) {
		try {
			return IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
