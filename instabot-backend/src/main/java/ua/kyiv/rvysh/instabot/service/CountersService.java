package ua.kyiv.rvysh.instabot.service;

import java.time.LocalDate;
import java.time.ZoneId;

import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.repository.CountersDao;

public class CountersService {
    private final CountersDao countersDao;

    public CountersService(CountersDao dao) {
        this.countersDao = dao;
    }

    public void insertFollowingCount(InstaBot bot, int count) {
        LocalDate now = LocalDate.now(ZoneId.of("UTC"));
        countersDao.insertFollowingCount(bot.getAccountName(), now, count);
    }

    public int getFollowingCount(InstaBot bot) {
        return getFollowingCount(bot.getAccountName());
    }

    public int getFollowingCount(String botName) {
        return getFollowingCount(botName, LocalDate.now(ZoneId.of("UTC")));
    }

    public int getFollowingCount(InstaBot bot, LocalDate date) {
        return getFollowingCount(bot.getAccountName(), date);
    }

    public int getFollowingCount(String botName, LocalDate date) {
        return countersDao.getFollowingCount(botName, date);
    }

    public void insertUnfollowCount(InstaBot bot, int count) {
        LocalDate now = LocalDate.now(ZoneId.of("UTC"));
        countersDao.insertUnfollowCount(bot.getAccountName(), now, count);
    }

    public int getUnfollowCount(InstaBot bot) {
        return getUnfollowCount(bot.getAccountName());
    }

    public int getUnfollowCount(String botName) {
        return getUnfollowCount(botName, LocalDate.now(ZoneId.of("UTC")));
    }

    public int getUnfollowCount(InstaBot bot, LocalDate date) {
        return getUnfollowCount(bot.getAccountName(), date);
    }

    public int getUnfollowCount(String botName, LocalDate date) {
        return countersDao.getUnfollowCount(botName, date);
    }
}
