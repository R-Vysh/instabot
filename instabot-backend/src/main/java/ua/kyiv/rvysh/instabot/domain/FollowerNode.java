package ua.kyiv.rvysh.instabot.domain;

public class FollowerNode {
	private Follower node;

	public Follower getNode() {
		return node;
	}

	public void setNode(Follower node) {
		this.node = node;
	}
}
