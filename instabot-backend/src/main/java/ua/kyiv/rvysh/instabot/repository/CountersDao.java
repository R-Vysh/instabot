package ua.kyiv.rvysh.instabot.repository;

import org.jooq.DSLContext;
import org.jooq.exception.NoDataFoundException;

import java.time.LocalDate;

import static ua.kyiv.rvysh.instabot.domain.generated.Tables.COUNTERS;

public class CountersDao {
    private final DSLContext dslContext;

    public CountersDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public void insertFollowingCount(String user, LocalDate date, int count) {
        dslContext.insertInto(COUNTERS, COUNTERS.USER, COUNTERS.DATE, COUNTERS.FOLLOWING_COUNT)
                .values(user, date.toString(), count)
                .onConflict(COUNTERS.USER, COUNTERS.DATE)
                .doUpdate()
                .set(COUNTERS.FOLLOWING_COUNT, count)
                .execute();
    }

    public int getFollowingCount(String user, LocalDate date) {
        try {
            return dslContext.select(COUNTERS.FOLLOWING_COUNT)
                    .from(COUNTERS)
                    .where(COUNTERS.USER.eq(user))
                    .and(COUNTERS.DATE.eq(date.toString()))
                    .fetchSingle()
                    .into(Integer.class);
        } catch (NoDataFoundException e) {
            return 0;
        }
    }

    public void insertUnfollowCount(String user, LocalDate date, int count) {
        dslContext.insertInto(COUNTERS, COUNTERS.USER, COUNTERS.DATE, COUNTERS.UNFOLLOW_COUNT)
                .values(user, date.toString(), count)
                .onConflict(COUNTERS.USER, COUNTERS.DATE)
                .doUpdate()
                .set(COUNTERS.UNFOLLOW_COUNT, count)
                .execute();
    }

    public int getUnfollowCount(String user, LocalDate date) {
        try {
            return dslContext.select(COUNTERS.UNFOLLOW_COUNT)
                    .from(COUNTERS)
                    .where(COUNTERS.USER.eq(user))
                    .and(COUNTERS.DATE.eq(date.toString()))
                    .fetchSingle()
                    .into(Integer.class);
        } catch (NoDataFoundException e) {
            return 0;
        }
    }
}
