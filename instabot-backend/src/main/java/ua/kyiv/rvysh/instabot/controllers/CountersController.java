package ua.kyiv.rvysh.instabot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kyiv.rvysh.instabot.service.CountersService;

@RestController
@RequestMapping("/counter")
public class CountersController {
	@Autowired
	private CountersService countersService;

	@GetMapping("/unfollow/{botName}")
	public int getUnfollowCount(@PathVariable("botName") String botName) {
		return countersService.getUnfollowCount(botName);
	}

	@GetMapping("/follow/{botName}")
	public int getFollowCount(@PathVariable("botName") String botName) {
		return countersService.getFollowingCount(botName);
	}
}
