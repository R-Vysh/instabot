package ua.kyiv.rvysh.instabot.utils;

public class NumberUtils {
	private NumberUtils() {
	}

	public static int parse(String number) {
		boolean isK = false;
		boolean isM = false;
		if (number.contains("k") || number.contains("K")) {
			isK = true;
		}
		if (number.contains("m") || number.contains("M")) {
			isM = true;
		}
		String str = number.replaceAll(" ", "")
				.replaceAll("k", "").replaceAll("K", "").replaceAll("m", "").replaceAll("M", "");
		if (isK || isM) {
			str = str.replaceAll(",", ".");
		} else {
			str = str.replaceAll(",", "");
		}
		double n = Double.parseDouble(str);
		if (isK) {
			n = n * 1_000;
		}
		if (isM) {
			n = n * 1_000_000;
		}
		return (int) n;
	}
}
