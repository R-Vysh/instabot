package ua.kyiv.rvysh.instabot.service;

import ua.kyiv.rvysh.instabot.InstaBot;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockService {
	private final Map<String, Lock> locks;
	
	public LockService() {
		locks = new HashMap<>();
	}

	public void lock(InstaBot bot) {
		lock(bot.getAccountName());
	}

	public void unlock(InstaBot bot) {
		unlock(bot.getAccountName());
	}

	public void lock(String lockName) {
		locks.putIfAbsent(lockName, new ReentrantLock());
		locks.get(lockName).lock();
	}

	public void unlock(String lockName) {
		locks.get(lockName).unlock();
	}

}
