package ua.kyiv.rvysh.instabot.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.lightbody.bmp.BrowserMobProxy;

public class DriverBundle {
	private final WebDriver driver;
	private final WebDriverWait wait;
	private final BrowserMobProxy proxy;
	
	public DriverBundle(WebDriver driver, WebDriverWait wait, BrowserMobProxy proxy) {
		this.driver = driver;
		this.wait = wait;
		this.proxy = proxy;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebDriverWait getWait() {
		return wait;
	}

	public BrowserMobProxy getProxy() {
		return proxy;
	}
}
