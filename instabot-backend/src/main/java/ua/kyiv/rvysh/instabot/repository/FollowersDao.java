package ua.kyiv.rvysh.instabot.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import ua.kyiv.rvysh.instabot.utils.ResourceUtils;

public class FollowersDao {
	private final JdbcTemplate jdbcTemplate;

	@Value("classpath:sql/followers-upsert.sql")
	private Resource upsertFollowers;

	public FollowersDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void insertFollowers(String user, List<String> followers) {
		jdbcTemplate.batchUpdate(ResourceUtils.toString(upsertFollowers),
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setString(1, user);
						ps.setString(2, followers.get(i));
					}

					@Override
					public int getBatchSize() {
						return followers.size();
					}
				});
	}
}
