package ua.kyiv.rvysh.instabot.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="Wrong bot credentials")
public class BotLoginException extends Exception {
	private static final long serialVersionUID = -5743268516368769825L;

}
