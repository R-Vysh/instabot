package ua.kyiv.rvysh.instabot.repository;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Result;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static ua.kyiv.rvysh.instabot.domain.generated.Tables.UNFOLLOW;

public class UnfollowDao {
    private final JdbcTemplate jdbcTemplate;
    private final DSLContext dslContext;

    public UnfollowDao(JdbcTemplate jdbcTemplate, DSLContext dslContext) {
        this.jdbcTemplate = jdbcTemplate;
        this.dslContext = dslContext;
    }

    public void insertUnfollow(String bot, String user, LocalDate date) {
        dslContext.insertInto(UNFOLLOW, UNFOLLOW.BOT, UNFOLLOW.USER, UNFOLLOW.DATETM)
                .values(bot, user, date.toString())
                .onConflictDoNothing()
                .execute();
    }

    public List<String> getUnfollow(String bot) {
        Result<Record1<String>> res = dslContext.select(UNFOLLOW.USER)
                .from(UNFOLLOW)
                .where(UNFOLLOW.BOT.eq(bot))
                .fetch();
        if (res != null) {
            return res.stream().map(r -> r.get(UNFOLLOW.USER)).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public Map<String, List<String>> getUnfollow(LocalDate olderThan) {
        try {
            Map<String, List<String>> result = new HashMap<>();
            List<Pair<String, String>> pairs = jdbcTemplate.query(
                    "SELECT bot, user FROM unfollow "
                            + "WHERE date(datetm)<? "
                            + "ORDER BY datetm",
                    new Object[]{olderThan},
                    new RowMapper<Pair<String, String>>() {
                        @Override
                        public Pair<String, String> mapRow(ResultSet rs, int rowNum)
                                throws SQLException {
                            return new ImmutablePair<String, String>(rs.getString("bot"),
                                    rs.getString("user"));
                        }

                    });
            for (Pair<String, String> pair : pairs) {
                result.putIfAbsent(pair.getKey(), new ArrayList<>());
                result.get(pair.getKey()).add(pair.getValue());
            }
            return result;
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyMap();
        }
    }

    public List<String> getUnfollow(String bot, LocalDate olderThan) {
        try {
            return jdbcTemplate.queryForList(
                    "SELECT user FROM unfollow WHERE bot=? AND date(datetm)<?",
                    new Object[]{bot, olderThan}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    public void deleteUnfollow(String bot, String user) {
        dslContext.deleteFrom(UNFOLLOW).where(UNFOLLOW.BOT.eq(bot)).and(UNFOLLOW.USER.eq(user)).execute();
    }
}
