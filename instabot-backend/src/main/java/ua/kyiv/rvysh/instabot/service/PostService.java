package ua.kyiv.rvysh.instabot.service;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.utils.Constants;
import ua.kyiv.rvysh.instabot.utils.RobotUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class PostService {
    private final String LOCK_UI_NAME = "__lock_ui__";

    private static final Logger LOG = LoggerFactory.getLogger(PostService.class);
    private static final String POST_BTN_XPATH = "//span[text()='New Post' or @aria-label='New Post']";

    private final LockService lockService;

    public PostService(LockService lockService) {
        this.lockService = lockService;
    }

    public synchronized void post(InstaBot bot, File pic, String text)
            throws InterruptedException {
        lockService.lock(LOCK_UI_NAME);
        try {
            LOG.info("Posting {}", pic.getAbsolutePath());
            WebDriverWait wait = bot.getDriverBundle().getWait();
            WebDriver driver = bot.getDriverBundle().getDriver();

            driver.get(Constants.BASE_URL);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(POST_BTN_XPATH))).click();
            RobotUtils.typeOnKeyboard(pic.getAbsolutePath(), true);
            TimeUnit.SECONDS.sleep(2);

            wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Next']")))
                    .click();
            TimeUnit.SECONDS.sleep(1);

            WebElement textInputField = wait.until(
                    ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(
                                    "//textarea[text()='Write a caption…' or @aria-label='Write a caption…']")));
            textInputField.click();
            textInputField.clear();
            textInputField.sendKeys(text);
            TimeUnit.SECONDS.sleep(1);
            wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Share']")))
                    .click();
        } finally {
            lockService.unlock(LOCK_UI_NAME);
        }
    }
}
