package ua.kyiv.rvysh.instabot.service;

import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.repository.UnfollowDao;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

public class UnfollowService {
    private final UnfollowDao unfollowDao;

    public UnfollowService(UnfollowDao dao) {
        this.unfollowDao = dao;
    }

    public void insertUnfollow(InstaBot bot, String user) {
        LocalDate now = LocalDate.now(ZoneId.of("UTC"));
        unfollowDao.insertUnfollow(bot.getAccountName(), user, now);
    }

    public List<String> getUnfollow(InstaBot bot) {
        return unfollowDao.getUnfollow(bot.getAccountName());
    }

    public List<String> getUnfollow(InstaBot bot, LocalDate olderThan) {
        return unfollowDao.getUnfollow(bot.getAccountName(), olderThan);
    }

    public void delete(InstaBot bot, String user) {
        delete(bot.getAccountName(), user);
    }

    public void delete(String botName, String user) {
        unfollowDao.deleteUnfollow(botName, user);
    }

    public Map<String, List<String>> getUnfollow(LocalDate olderThan) {
        return unfollowDao.getUnfollow(olderThan);
    }
}
