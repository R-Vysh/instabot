package ua.kyiv.rvysh.instabot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kyiv.rvysh.instabot.domain.exception.BotLoginException;
import ua.kyiv.rvysh.instabot.service.CookiesService;

import java.io.IOException;

@RestController
@RequestMapping("/cookies")
public class CookiesController {

    @Autowired
    private CookiesService cookiesService;

    @GetMapping("/isValid/{botName}")
    public boolean checkValidity(@PathVariable("botName") String botName) {
        return cookiesService.isCookieValid(botName);
    }
}
