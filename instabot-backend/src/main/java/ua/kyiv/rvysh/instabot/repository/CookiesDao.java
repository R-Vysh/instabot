package ua.kyiv.rvysh.instabot.repository;

import org.jasypt.util.text.TextEncryptor;
import org.jooq.DSLContext;
import org.jooq.Record;
import ua.kyiv.rvysh.instabot.domain.Cookiez;

import java.time.ZonedDateTime;

import static ua.kyiv.rvysh.instabot.domain.generated.Tables.COOKIES;

public class CookiesDao {
    private final DSLContext dslContext;
    private final TextEncryptor encryptor;

    public CookiesDao(DSLContext dslContext, TextEncryptor encryptor) {
        this.dslContext = dslContext;
        this.encryptor = encryptor;
    }

    public Cookiez getCookies(String user) {
        Record r = dslContext.select(COOKIES.COOKIES_, COOKIES.EXPIRES_ON)
                .from(COOKIES)
                .where(COOKIES.USER.eq(user))
                .fetchOne();
        if (r != null) {
            return new Cookiez(
                    encryptor.decrypt(r.get(COOKIES.COOKIES_)), ZonedDateTime.parse(r.get(COOKIES.EXPIRES_ON)));
        } else {
            return null;
        }
    }

    public void insertCookies(String user, String cookies, ZonedDateTime expiresOn) {
        dslContext.insertInto(COOKIES, COOKIES.USER, COOKIES.COOKIES_, COOKIES.EXPIRES_ON)
                .values(user, encryptor.encrypt(cookies), expiresOn.toString())
                .onConflict(COOKIES.USER)
                .doUpdate()
                .set(COOKIES.COOKIES_, encryptor.encrypt(cookies))
                .set(COOKIES.EXPIRES_ON, expiresOn.toString())
                .execute();
    }
}
