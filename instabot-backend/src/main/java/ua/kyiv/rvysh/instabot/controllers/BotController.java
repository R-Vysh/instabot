package ua.kyiv.rvysh.instabot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ua.kyiv.rvysh.instabot.domain.BotCredentials;
import ua.kyiv.rvysh.instabot.domain.exception.BotLoginException;
import ua.kyiv.rvysh.instabot.service.BotService;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("/bot")
public class BotController {

    @Autowired
    private BotService botService;

    @Value("${follow.limit}")
    private int followLimit;

    @PostMapping("/add")
    public void add(@RequestBody BotCredentials creds) throws BotLoginException {
        botService.addBot(creds, followLimit);
    }

    @PostMapping("/start/{botName}")
    public void start(@PathVariable("botName") String botName)
            throws BotLoginException {
        botService.startBot(botName);
    }

    @PostMapping("/restart/{botName}")
    public void restart(@PathVariable("botName") String botName) throws BotLoginException {
        botService.restart(botService.get(botName));
    }

    @PostMapping("/enqueue/{botName}/{donor}")
    public void followEnqueue(@PathVariable("botName") String botName,
                              @PathVariable("donor") String donorPage,
                              @RequestParam(name = "size") int size)
            throws InterruptedException, IOException {
        botService.enqueueFollowers(botService.get(botName), donorPage, size);
    }

    @PostMapping("/start-follow/{botName}")
    public void startFollow(@PathVariable("botName") String botName) throws IOException {
        botService.processFollowQueues(botService.get(botName));
    }

    @PostMapping("/enqueue-unfollow/{botName}")
    public void unfollowEnqueue(@PathVariable("botName") String botName,
                                @RequestParam(name = "size") int size) throws InterruptedException, IOException {
        botService.enqueueUnfollow(botService.get(botName), size);
    }

    @PostMapping("/start-unfollow/{botName}")
    public void startUnfollow(@PathVariable("botName") String botName) throws IOException {
        botService.processUnfollowQueue(botService.get(botName));
    }

    @GetMapping("/isStarted/{botName}")
    @ResponseBody
    public boolean isStarted(@PathVariable("botName") String botName) {
        return botService.listRunning().contains(botName);
    }

    @GetMapping("/listAll")
    @ResponseBody
    public Set<String> listAll() {
        return botService.listAll();
    }

    @GetMapping("/listRunning")
    @ResponseBody
    public Set<String> listRunning() {
        return botService.listRunning();
    }

    // @PostMapping("/autopost/{botName}")
    // public ResponseEntity<Void> autoPost(@PathVariable("botName") String botName)
    // throws InterruptedException, IOException {
    // botService.get(botName).schedulePosting(new File("/home/ros/Desktop/1.png"),
    // "YO");
    // return new ResponseEntity<>(HttpStatus.OK);
    // }
}
