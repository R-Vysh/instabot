package ua.kyiv.rvysh.instabot.filter;

public interface Filter {
	public boolean filter(String text);
}
