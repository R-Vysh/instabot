package ua.kyiv.rvysh.instabot.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ua.kyiv.rvysh.instabot.domain.QueueInfo;
import ua.kyiv.rvysh.instabot.service.QueueService;

@RestController
@RequestMapping("/queue")
public class QueuesController {
	@Autowired
	private QueueService queueService;

	@GetMapping("/{botName}")
	public List<QueueInfo> getQueues(@PathVariable("botName") String botName) throws IOException {
		return queueService.getQueuesInfo(botName);
	}
}
