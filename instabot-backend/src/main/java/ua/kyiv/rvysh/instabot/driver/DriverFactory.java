package ua.kyiv.rvysh.instabot.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;

public class DriverFactory {
    @Value("${selenium.headless}")
    private boolean headless;

    @Value("${selenium.load.images}")
    private boolean loadImages;

    public DriverBundle getDriver(DriverType type) {
        switch (type) {
            case FIREFOX:
                return driverFirefox();
            case CHROME:
            default:
                return driverChrome();

        }
    }

    private DriverBundle driverChrome() {
        BrowserMobProxyServer bmProxy = new BrowserMobProxyServer();
        bmProxy.start(0);
        bmProxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
        Proxy proxy = ClientUtil.createSeleniumProxy(bmProxy);

        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "iPhone X");

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en", "--window-size=375,812");
        if (!loadImages) {
            options.addArguments("--blink-settings=imagesEnabled=false");
        }
        if (headless) {
            options.addArguments("--headless");
        }
        options.setCapability("proxy", proxy);
        options.setExperimentalOption("mobileEmulation", mobileEmulation);

        WebDriver driver = new ChromeDriver(options);
        return new DriverBundle(driver, new WebDriverWait(driver, 10), bmProxy);
    }

    private DriverBundle driverFirefox() {
        BrowserMobProxyServer bmProxy = new BrowserMobProxyServer();
        bmProxy.start(0);
        bmProxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
        Proxy proxy = ClientUtil.createSeleniumProxy(bmProxy);
        WebDriverManager.firefoxdriver().setup();

        FirefoxProfile profile = new FirefoxProfile();

        profile.setPreference("general.useragent.override",
                "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 " +
                        "(KHTML, like Gecko) FxiOS/18.1 Mobile/16B92 Safari/605.1.15");
        profile.setPreference("intl.accept_languages", "en");
        profile.setPreference("media.volume_scale", "0.0");
        if (!loadImages) {
            profile.setPreference("permissions.default.image", 2);
        }
        FirefoxOptions options = new FirefoxOptions();
        options.setCapability("proxy", proxy);
        options.setProfile(profile);
        options.setHeadless(headless);

        WebDriver driver = new FirefoxDriver(options);
        driver.manage().window().setSize(new Dimension(375, 812));
        return new DriverBundle(driver, new WebDriverWait(driver, 10), bmProxy);
    }
}
