package ua.kyiv.rvysh.instabot.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kyiv.rvysh.instabot.InstaBot;
import ua.kyiv.rvysh.instabot.driver.DriverBundle;
import ua.kyiv.rvysh.instabot.utils.Constants;

public class LikeService {
    private static final Logger LOG = LoggerFactory.getLogger(LikeService.class);

    private final LockService lockService;

    public LikeService(LockService lockService) {
        this.lockService = lockService;
    }

    public void like(InstaBot bot, String user) throws InterruptedException {
        lockService.lock(bot);
        try {
            DriverBundle driverBundle = bot.getDriverBundle();
            WebDriverWait wait = driverBundle.getWait();
            WebDriver driver = driverBundle.getDriver();

            LOG.info("Like {}", user);
            driver.get(Constants.BASE_URL + user + "/feed");
            List<WebElement> likes = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                    By.xpath("//article//button//span[@aria-label = 'Like']")));
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView({block: \"center\", inline: \"center\"});",
                    likes.get(0));
            likes.get(0).click();
            TimeUnit.SECONDS.sleep(5);
        } finally {
            lockService.unlock(bot);
        }
    }
}