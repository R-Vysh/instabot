package ua.kyiv.rvysh.instabot.domain;

public class BotCredentials {
	private String accName;
	private char[] password;
	
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public char[] getPassword() {
		return password;
	}
	public void setPassword(char[] password) {
		this.password = password;
	}
}
