package ua.kyiv.rvysh.instabot.filter;

import java.util.ArrayList;
import java.util.List;

public class FilterChain {
	private List<Filter> filters;
	
	private FilterChain(List<Filter> filters) {
		this.filters = filters;
	}
	
	public boolean filter(String text) {
		boolean res = true;
		for (Filter filter : filters) {
			res = res && filter.filter(text);
		}
		return res;
	}
	
	public static class Builder {
		private List<Filter> filters = new ArrayList<>();
		
		public Builder addFilter(Filter f) {
			filters.add(f);
			return this;
		}
		
		public FilterChain build() {
			return new FilterChain(filters);
		}
	}
}
