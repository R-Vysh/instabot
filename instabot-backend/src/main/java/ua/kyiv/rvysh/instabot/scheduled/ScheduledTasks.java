package ua.kyiv.rvysh.instabot.scheduled;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import ua.kyiv.rvysh.instabot.service.QueueService;
import ua.kyiv.rvysh.instabot.service.UnfollowService;

public class ScheduledTasks {
	private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);

	private final UnfollowService unfollowService;
	private final QueueService queueService;
	
	public ScheduledTasks(UnfollowService unfollowService, QueueService queueService) {
		this.unfollowService = unfollowService;
		this.queueService = queueService;
	}
	
	@Scheduled(fixedRate = 24 * 3600 * 1000, initialDelay = 5_000)
	public void enqueueUnschedule() throws IOException {
		LOG.info("Filling in unfollow queues");
		LocalDate date = LocalDate.now(ZoneId.of("UTC")).minusDays(2L);
		Map<String, List<String>> unfollows = unfollowService.getUnfollow(date);
		for(Map.Entry<String, List<String>> entry : unfollows.entrySet()) {
			queueService.getUnfollowQueue(entry.getKey()).addAll(entry.getValue());
			for(String s : entry.getValue()) {
				unfollowService.delete(entry.getKey(), s);
			}
		}
	}
}
