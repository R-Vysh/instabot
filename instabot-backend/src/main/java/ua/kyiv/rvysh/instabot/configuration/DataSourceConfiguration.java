package ua.kyiv.rvysh.instabot.configuration;

import org.apache.commons.dbcp2.BasicDataSource;
import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jooq.JooqExceptionTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DelegatingDataSource;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import ua.kyiv.rvysh.instabot.utils.ResourceUtils;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Value("classpath:sql/create/create-bots.sql")
    private Resource initBots;

    @Value("classpath:sql/create/create-cookies.sql")
    private Resource initCookies;

    @Value("classpath:sql/create/create-followers.sql")
    private Resource initFollowers;

    @Value("classpath:sql/create/create-counters.sql")
    private Resource initCounters;

    @Value("classpath:sql/create/create-unfollow.sql")
    private Resource initUnfollow;

    @Bean
    public DataSource dataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.sqlite.JDBC");
        ds.setUrl("jdbc:sqlite:instabot.db");
        ds.setInitialSize(2);
        ds.setMaxTotal(10);
        return ds;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate template = new JdbcTemplate(dataSource());
        template.execute(ResourceUtils.toString(initBots));
        template.execute(ResourceUtils.toString(initCookies));
        template.execute(ResourceUtils.toString(initFollowers));
        template.execute(ResourceUtils.toString(initCounters));
        template.execute(ResourceUtils.toString(initUnfollow));
        return template;
    }


    @Bean
    public DataSourceTransactionManager transactionManager() {
        DataSourceTransactionManager tm = new DataSourceTransactionManager();
        tm.setDataSource(dataSource());
        return tm;
    }

    @Bean
    public DelegatingDataSource transactionAwareDataSourceProxy() {
        return new TransactionAwareDataSourceProxy(dataSource());
    }

    @Bean
    public ConnectionProvider connectionProvider() {
        return new DataSourceConnectionProvider(transactionAwareDataSourceProxy());
    }

    @Bean
    public org.jooq.Configuration jooqConfig() {
        DefaultConfiguration config = new DefaultConfiguration();
        config.setSQLDialect(SQLDialect.SQLITE);
        config.setConnectionProvider(connectionProvider());
        config.setExecuteListenerProvider(new DefaultExecuteListenerProvider(new JooqExceptionTranslator()));
        return config;
    }

    @Bean
    public DSLContext jooqContext() {
        return new DefaultDSLContext(jooqConfig());
    }
}
