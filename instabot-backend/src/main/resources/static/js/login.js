$(document).ready(function(user, pass) {
  var login = function() {
    $.ajax({
      url: getHost() + "/login/login.json?j_username=" + user + "&j_password=" + pass,
      type: "POST",
      data: $("#loginForm").serialize(),
      success: function(data, status) {
      }
    });
  };

  $("#loginBtn").click(function() {
    login($("#username").val(), $("#password").val());
  });
});
