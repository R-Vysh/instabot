$(document).ready(function() {

  var countersDiv = $("#counters");
  var queuesDiv = $("#queues");
  var controlsDiv = $("#controls");
  var startBtn = $("#startBot");
  var restartBtn = $("#restartBot");
  var loading = $("#loading");
  var botSelector = $("#botSelector");
  var addBotBtn = $("#addBot");
  var addDonorBtn = $("#addDonor");
  var enqueueUnfollowBtn = $("#enqueueUnfollow");
  var startFollowBtn = $("#startFollow");
  var accNameInput = $("#accName");
  var passwordInput = $("#password");
  var startUnfollowBtn = $("#startUnfollow");
  var loadingText = $("#loadingText");

  var saveBot = function(botName, pass) {
    loading.show();
    $.ajax({
      contentType: 'application/json',
      method: "POST",
      url: "/bot/add",
      data: JSON.stringify({
        "accName": botName,
        "password": pass
      }),
      success: function(data) {
        loadAllBots(botName);
        loading.hide();
      },
      error: function() {
        alert("Failed to login");
      }
    });
    $("#closeAddBotPopup").click();
  };

  var restartBot = function(botName) {
    loading.show();
    $.ajax({
      contentType: 'application/json',
      method: "POST",
      url: "/bot/restart/" + botName,
      success: function(data) {
        loadAllBots(botName);
        loading.hide();
      },
      error: function() {
        alert("Failed to restart bot");
      }
    });
  };

  var startBot = function(botName) {
    loading.show();
    $.ajax({
      contentType: 'application/json',
      method: "POST",
      url: "/bot/start/" + botName,
      success: function(data) {
        loadAllBots(botName);
        loading.hide();
      },
      error: function() {
        alert("Failed to start bot");
      }
    });
  };

  var checkStarted = function(botName) {
    $.ajax({
      method: "GET",
      url: "/bot/isStarted/" + botName,
      success: function(data) {
        if(data === true) {
          startBtn.hide();
          restartBtn.show();
          loadingText.text("");
          loading.hide();
        } else {
          startBtn.show();
          restartBtn.hide();
          loadingText.text("Бот не запущений");
          loading.show();
        }
      }
    });
  }

  var loadAllBots = function(activeBot) {
    $.ajax({
      method: "GET",
      url: "/bot/listAll",
      success: function(data) {
        var currentBot = activeBot === null ? data[0] : activeBot;
        $("#botSelector > option:enabled").remove();
        data.forEach(function(botName) {
          botSelector.append($('<option>', {
            value: botName,
            text : botName
          }));
        });
        $("#botSelector > option").filter(function() {
          return ($(this).val() == currentBot);
        }).prop('selected', true);
        botSelector.change();
      }
    });
  };

  var loadQueues = function(botName) {
    $.ajax({
      method: "GET",
      url: "/queue/" + botName,
      success: function(data) {
        $("#queues > tbody > tr").remove();
        data.forEach(function(queue) {
          $("#queues > tbody:last-child").append("<tr><td>" + queue.name + "</td><td>" + queue.size + "</td></tr>");
        });
      }
    });
  };

  var enqueueFollow = function(user, donor) {
    loading.show();
    $.ajax({
      method: "POST",
      url: "/bot/enqueue/" + user + "/" + donor,
      data: jQuery.param({
        size: $("#donorSize").val()
      }),
      success: function(data) {
        loadQueues(user);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
      },
      complete: function(xhr, status) {
        loading.hide();
      }
    });
  };

  var enqueueUnfollow = function(user) {
    loading.show();
    $.ajax({
      method: "POST",
      url: "/bot/enqueue-unfollow/" + user,
      data: jQuery.param({
        size: $("#unfollowSize").val()
      }),
      success: function(data) {
        loadQueues(user);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
      },
      complete: function(xhr, status) {
        loading.hide();
      }
    });
  };

  var startFollow = function(user) {
    loading.show();
    $.ajax({
      method: "POST",
      url: "/bot/start-follow/" + user,
      complete: function(xhr, status) {
        loading.hide();
      }
    });
  };

  var startUnfollow = function(user) {
    loading.show();
    $.ajax({
      method: "POST",
      url: "/bot/start-unfollow/" + user,
      complete: function(xhr, status) {
        loading.hide();
      }
    });
  };

  var autopost = function(user) {
    $.ajax({
      method: "POST",
      url: "/bot/autopost/" + user,
      success: function(data) {}
    });
  };

  var loadFollowCount = function(user) {
    $.ajax({
      method: "GET",
      url: "/counter/follow/" + user,
      success: function(data) {
        $("#followedCount").text(data);
      }
    });
  };

  var loadUnfollowCount = function(user) {
    $.ajax({
      method: "GET",
      url: "/counter/unfollow/" + user,
      success: function(data) {
        $("#unfollowedCount").text(data);
      }
    });
  };

  var getCurrentBotName = function() {
    return $("#botSelector option:selected").val();
  }

  var checkCookiesValidity = function(user) {
    $.ajax({
      method: "GET",
      url: "/cookies/isValid/" + user,
      success: function(data) {
        if(data === true) {
          $("#checkCookies").text("Користувач " + user + " залогінений. Пароль не потрібен.");
        }
      }
    });
  }

  botSelector.change(function() {
    if (getCurrentBotName()) {
      countersDiv.show();
      queuesDiv.show();
      controlsDiv.show();
      loadQueues(getCurrentBotName());
      checkStarted(getCurrentBotName());
    } else {
      countersDiv.hide();
      queuesDiv.hide();
      controlsDiv.hide();
      startBtn.hide();
      restartBtn.hide();
    }
  });

  addBotBtn.click(function() {
    saveBot(accNameInput.val(), passwordInput.val());
  });

  startBtn.click(function() {
    startBot(getCurrentBotName());
  });

  restartBtn.click(function() {
    restartBot(getCurrentBotName());
  });

  addDonorBtn.click(function() {
    enqueueFollow(getCurrentBotName(), $("#donor").val());
  });

  enqueueUnfollowBtn.click(function() {
    enqueueUnfollow(getCurrentBotName());
  });

  startFollowBtn.click(function() {
    startFollow(getCurrentBotName());
  });

  startUnfollowBtn.click(function() {
    startUnfollow(getCurrentBotName());
  });

  $("#autopost").click(function() {
    autopost(getCurrentBotName());
  });

  passwordInput.focus(function () {
    checkCookiesValidity(accNameInput.val());
  });

  passwordInput.keypress(function (e) {
    if (e.which == 13) {
      addBotBtn.click();
      return false;
    }
  });

  loadAllBots(null);
  botSelector.change();

  setInterval(function() {
    loadFollowCount(getCurrentBotName())
  }, 10000);
  setInterval(function() {
    loadUnfollowCount(getCurrentBotName())
  }, 10000);
});
