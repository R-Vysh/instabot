CREATE TABLE IF NOT EXISTS followers 
(id INTEGER PRIMARY KEY AUTOINCREMENT,
 user TEXT,
 follower TEXT,
 UNIQUE(user, follower));
