FROM gcr.io/google-appengine/openjdk:8
COPY ./build/libs/instabot-*.jar /opt/apps/instabot/instabot.jar
EXPOSE 8080
ENTRYPOINT java -jar /opt/apps/instabot/instabot.jar
